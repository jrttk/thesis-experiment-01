let wavesurferConfig = {
  container: document.querySelector('#waveform'),
  waveColor: '#8B256B',
  progressColor: '#FF007A',
  cursorColor: '#FF007A',
  barWidth: 2,
  barRadius: 4,
  cursorWidth: 1,
  barGap: 3,
}
let wavesurfer = WaveSurfer.create(wavesurferConfig)

wavesurfer.on('error', (e) => {
  console.warn(e)
})

wavesurfer.on('ready', () => {
  emptyHandler()
  setNewHeight()
})

// Mounted
window.addEventListener('DOMContentLoaded', () => {
  emptyHandler()

  let waveform = document.getElementById('waveform')
  wavesurfer.setHeight(waveform.offsetHeight)

  // Get Microphone
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    // console.log('getUserMedia supported.')
    navigator.mediaDevices
      .getUserMedia(
        // constraints - only audio needed for this app
        {
          audio: true,
        }
      )

      // Success callback
      .then(function (stream) {
        const mediaRecorder = new MediaRecorder(stream)
        const recordButton = document.getElementById('recordButton')
        const clearButton = document.getElementById('clearButton')

        recordButton.onclick = () => {
          let recordButton = document.getElementById('recordButton')
          let active = recordButton.classList.contains(
            'recorder__button--active'
          )
          if (!active) {
            recordButton.classList.add('recorder__button--active')
            mediaRecorder.start()
          } else {
            recordButton.classList.remove('recorder__button--active')
            mediaRecorder.stop()
          }
        }

        let chunks = []

        mediaRecorder.ondataavailable = (e) => {
          chunks.push(e.data)
          console.log(e.data)
          const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' })
          const audioURL = window.URL.createObjectURL(blob)
          wavesurfer.load(audioURL)
          emptyHandler()
        }

        mediaRecorder.onstop = () => {
          const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' })
          chunks = []
          const audioURL = window.URL.createObjectURL(blob)
          wavesurfer.load(audioURL)
        }

        clearButton.onclick = () => {
          // console.log(mediaRecorder.state)
          if (mediaRecorder.state === 'recording') {
            // console.log('stopping')
            mediaRecorder.stop()
          }

          chunks = []
          let recordButton = document.getElementById('recordButton')
          recordButton.classList.remove('recorder__button--active')
          wavesurfer.destroy()
          wavesurfer = WaveSurfer.create(wavesurferConfig)
          emptyHandler()
        }
      })

      // Error callback
      .catch(function (err) {
        console.log('The following getUserMedia error occurred: ' + err)
      })
  } else {
    console.log('getUserMedia not supported on your browser!')
  }
})

// Player's function
const playAudio = () => wavesurfer.play()
const pauseAudio = () => wavesurfer.pause()
const stopAudio = () => wavesurfer.stop()
const emptyHandler = () => {
  let waveform = document.getElementById('waveform')
  let emptyNotice = document.getElementById('emptyNotice')
  // console.log(wavesurfer.getDuration())

  if (wavesurfer.getDuration() === 0) {
    waveform.classList.add('--hidden')
    emptyNotice.classList.add('recorder__wave__empty--active')
  } else {
    waveform.classList.remove('--hidden')
    emptyNotice.classList.remove('recorder__wave__empty--active')
  }
}
const setNewHeight = () => {
  let waveform = document.getElementById('waveform')
  // console.log(waveform.offsetHeight)
  wavesurfer.setHeight(waveform.offsetHeight)
}

window.addEventListener('resize', () => {
  setNewHeight()
})
